import React, { useState } from "react";
import { Button } from 'react-bootstrap';

const Buttons = ({color,onClick,text}) => {

  return (
    <>
      <Button onClick={onClick} variant={color} color='#fff'>{text}</Button>
    </>
  );
};

export default Buttons;