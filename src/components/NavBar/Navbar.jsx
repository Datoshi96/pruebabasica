import React from "react";
import "./Navbar.css";

const Navbar = ({change}) => {
  return (
    <nav className="navbar-home">
      <h1 className={change === 1 ? 'navbar-logo': change === 2 ? 'navbar-logo-blue': change === 3 && 'navbar-logo-red'}>
        Prueba Básica
      </h1>
    </nav>
  );
};

export default Navbar;