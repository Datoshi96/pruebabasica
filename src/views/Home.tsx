import { useEffect, useState } from "react"
import Container from 'react-bootstrap/Container';
import Buttons from "../components/Buttons/Buttons";
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Navbar from "../components/NavBar/Navbar";

const Home = () =>{
    const [change, setChange] = useState<number>(1)
    const [show, setShow] = useState<boolean>(false);
    const [message, setMessage] = useState<string>()
    
    const onChangeColorBlue = () => {
        setMessage("Señor usuario el color de la pantalla ha cambiado a azul")
        setChange(2)
        setShow(true)
    }
    const onChangeColorRed = () => {
        setMessage("Señor usuario el color de la pantalla ha cambiado a rojo")
        setChange(3)
        setShow(true)
    }

    return(
        <>
        <Navbar change={change}></Navbar>
        <Container >
            <Row className="mt-5">
                <Col>
                    <Buttons onClick={onChangeColorBlue} text={'Azul'} color='primary'/>
                </Col>
                <Col>
                    <Buttons onClick={onChangeColorRed} text={'Rojo'} color='danger'/>
                </Col>
                
            </Row>
        </Container>
        <Modal
        show={show}
        onHide={() => setShow(false)}
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body>
            {message}
        </Modal.Body>
      </Modal>
        </>
    )

};

export default Home;