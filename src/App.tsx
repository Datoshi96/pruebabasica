import React from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from './components/NavBar/Navbar';
import Home from './views/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  return (
    <div className="App">
      <Home />
      
    </div>
  );
}

export default App;
